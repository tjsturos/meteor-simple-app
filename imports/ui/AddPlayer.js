import React from 'react';
import {Players} from './../../imports/api/players';

export default class AddPlayer extends React.Component {
  handleSubmit(e) {
    let playerName = e.target.playerName.value;

    e.preventDefault();

    if(playerName.trim()) {
      e.target.playerName.value = "";
      Players.insert({
          name: playerName,
          score: 0
      });
    } else e.target.playerName.value = ""; 
  }
  render() {
    return (
      <div className="item">
      <form className="form" onSubmit={this.handleSubmit}>
        <input className="form__input" type="text" name="playerName" placeholder="Player Name" />
        <button className="button">Add Player</button>
      </form>
     </div> 
    );
  }
};